# Copyright (C) 2015 Patrick Farrell
#
# This file is a supplemental material to the paper
# Linear and nonlinear solvers for variational phase-field models of brittle fracture
# by P. E. Farrell and C. Maurini
#
# varfrac-solvers is free software: you can redistribute it and/or modify
# it under the terms of the GNU Lesser General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# varfrac-solvers is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
# GNU Lesser General Public License for more details.
#
# You should have received a copy of the GNU Lesser General Public License
# along with varfrac-solvers. If not, see <http://www.gnu.org/licenses/>.

from petsc4py import PETSc
from petsctools import get_residual_norm_vi, set_snes_linear_its
from mpi4py import MPI
from dolfin import info_blue, info_green, info_red

class SubSnes(object):
    def __init__(self, problem, snes, eqn, var, name):
        self.problem = problem
        self.parent = snes
        self.eqn = eqn
        self.var = var
        self.name = name

        assert self.eqn.size == self.var.size

        parent_x = self.parent.getSolution()
        self.parent_t1 = parent_x.duplicate()
        self.parent_t2 = parent_x.duplicate()

    def residual(self, obj, x, b):
        """ SNES residual function. """
        parent_x = self.parent.getSolution()
        parent_x.isset(self.var, 0.0)
        parent_x.isaxpy(self.var, 1.0, x)

        parent_f = self.parent.getFunction()[0]
        self.parent.computeFunction(parent_x, parent_f)
        child_f = parent_f.getSubVector(self.eqn)
        b.zeroEntries()
        b.axpy(1.0, child_f)
        parent_f.restoreSubVector(self.eqn, child_f)

    def jacobian(self, snes, x, J, P):
        """ SNES Jacobian function. """
        parent_x = self.parent.getSolution()
        parent_x.isset(self.var, 0.0)
        parent_x.isaxpy(self.var, 1.0, x)

        self.problem.set_nfs_subindices(self.eqn, self.var)
        self.problem.set_pc_prefix(self.name)

        orig_snes = self.problem.get_solver()
        if orig_snes != snes:
            self.orig_snes = orig_snes
            self.problem.set_solver(snes)

        parent_J = self.parent.getJacobian()[0]
        self.parent.computeJacobian(parent_x, parent_J)

        parent_J.getSubMatrix(self.eqn, self.var, J)

    def mult(self, obj, x, y):
        """ Mat matrix-vector product function. """
        self.parent_t1.zeroEntries()
        self.parent_t1.isaxpy(self.var, 1.0, x)

        parent_J = self.parent.getJacobian()[0]
        parent_J.mult(self.parent_t1, self.parent_t2)

        child_y = self.parent_t2.getSubVector(self.eqn)
        y.zeroEntries()
        y.axpy(1.0, child_y)
        self.parent_t2.restoreSubVector(self.eqn, child_y)

    def apply(self, obj, x, y):
        """ PC preconditioner action. """
        self.problem.pc_apply(x, y)

    def postsolve(self):
        self.problem.set_nfs_subindices(None, None)
        self.problem.set_inact_subindices(None)
        self.problem.set_pc_prefix("inner_")
        if hasattr(self, 'orig_snes'):
            self.problem.set_solver(self.orig_snes)
            del self.orig_snes

class NonlinearGaussSeidel(object):
    def __init__(self, problem, equations, variables=None, overrelaxation=1.0):
        # Equations: a list of index sets defining the equations to be considered in turn
        # Variables: a list of index sets defining the variables to be updated in each subsolve
        # Obviously, equations[i] must have the same size as variables[i]
        self.equations = equations
        self.variables = variables or equations
        assert len(self.equations) == len(self.variables)
        for (eqn, var) in zip(self.equations, self.variables):
            assert eqn.getSizes() == var.getSizes()

        self.initialised = False

        self.problem = problem

        self.log = PETSc.Log().Stage("Nonlinear Gauss-Seidel")
        self.overrelaxation = overrelaxation

    def setup(self, snes):
        self.initialised = True

        nsplit = len(self.equations)
        self.nsplit = nsplit

        parent_f = snes.getFunction()[0]
        parent_name = snes.getName() or ""
        if len(parent_name) > 0 and not parent_name.endswith("_"):
            parent_name += "_"
        parent_prefix = snes.getOptionsPrefix() or ""
        if len(parent_prefix) > 0 and not parent_prefix.endswith("_"):
            parent_prefix += "_"

        self.subsnes = []
        self.contexts = []
        self.sneslogs = []
        self.snesiters = [0] * nsplit

        for i in range(nsplit):
            eqn = self.equations[i]
            var = self.variables[i]

            obj = SubSnes(self.problem, snes, eqn, var, parent_prefix + "nfieldsplit_%d_" % i)
            self.contexts.append(obj)
            snes_ = PETSc.SNES().create(snes.getComm())
            snes_.incrementTabLevel(1, snes)

            child_f = parent_f.getSubVector(eqn)
            snes_.setFunction(obj.residual, child_f.duplicate())
            parent_f.restoreSubVector(eqn, child_f)

            sizes = eqn.getSizes()
            parentJ = self.problem._J.mat()
            subJ = parentJ.getSubMatrix(eqn, var)
            snes_.setJacobian(obj.jacobian, subJ)

            snes_.setName(parent_name + "nfieldsplit_%d_" % i)
            snes_.setOptionsPrefix(parent_prefix + "nfieldsplit_%d_" % i)
            snes_.setFromOptions()

            try:
                snes_.setCountersReset(False)
            except:
                snes_.setResetCounters(False)

            snes_.ksp.pc.setType("python")
            snes_.ksp.pc.setPythonContext(obj)

            if snes_.getType().startswith("vinewton"):
                assert hasattr(self.problem, 'lb')
                assert hasattr(self.problem, 'ub')
                child_lb = self.problem.lb.getSubVector(eqn)
                child_ub = self.problem.ub.getSubVector(eqn)
                snes_.setVariableBounds(child_lb.copy(), child_ub.copy())
                self.problem.lb.restoreSubVector(eqn, child_lb)
                self.problem.ub.restoreSubVector(eqn, child_ub)

            self.subsnes.append(snes_)

            sublog = PETSc.Log().Stage("Subsolve %d of nonlinear Gauss-Seidel (%s)" % (i, snes_.getType()))
            self.sneslogs.append(sublog)

            self.snesiters[i] = 0

    def solve(self, snes, b, x):
        # First, let's initialise. Initialise first from index sets supplied in the
        # constructor, otherwise try to figure it out from the objects.
        self.log.push()

        # FIXME: have DOLFIN call VecSetBlockSize.

        if not self.initialised:
            self.setup(snes)

        # OK! Let's do some nonlinear Gauss-Seidel.

        # This case *could* be handled, but I haven't thought about it -- do I have to explicitly
        # subtract b from the residual myself in the residual norms?
        assert b is None

        f = snes.getFunction()[0]
        snes.computeFunction(x, f)
        snes.reason = PETSc.SNES.ConvergedReason.CONVERGED_ITERATING

        its   = 0
        lits  = 0
        snes.setIterationNumber(its)

        bs = []
        for eqn in self.equations:
            if b is not None:
                child_b = b.getSubVector(eqn)
                bs.append(child_b.copy())
                b.restoreSubVector(child_b)
            else:
                bs.append(None)

        old_x = x.copy()
        old_f = f.copy()
        y = snes.getSolutionUpdate()
        y.zeroEntries()

        x.normBegin()
        f.normBegin()
        xnorm = x.normEnd()
        fnorm = f.normEnd()
        ynorm = 0.0

        if hasattr(self.problem, 'lb'):
            fnorm = get_residual_norm_vi(f, x, self.problem.lb, self.problem.ub)

        snes.reason = snes.callConvergenceTest(its, xnorm, ynorm, fnorm)
        snes.logConvergenceHistory(fnorm, lits)
        snes.monitor(its, fnorm)

        while True:
            if snes.reason: break
            if its == snes.max_it:
                snes.reason = PETSc.SNES.ConvergedReason.DIVERGED_MAX_IT
                break

            f.copy(old_f)

            for (i, var, snes_, context_, b_, sublog) in zip(range(self.nsplit), self.variables, self.subsnes, self.contexts, bs, self.sneslogs):
                sublog.push()
                child_x = x.getSubVector(var)
                x_ = child_x.copy()
                x.restoreSubVector(var, child_x)

                lits -= snes_.getLinearSolveIterations()

                snes_.solve(b_, x_)
                context_.postsolve()

                lits += snes_.getLinearSolveIterations()
                sublog.pop()

                self.snesiters[i] += snes_.its

                child_old_x = old_x.getSubVector(var)
                child_diff = x_.duplicate()
                child_diff.axpy(-1.0, child_old_x)
                child_diff.axpy(+1.0, x_)

                omega = self.overrelaxation
                while True:
                    x.isset(var, 0.0)
                    x.isaxpy(var, 1.0, child_old_x)
                    x.isaxpy(var, omega, child_diff)

                    if omega <= 1.0 or self.check_bounds(x):  # feasible
                        break
                    else:                        # infeasible
                        info_red("(sub %s) omega = %s yields infeasible step" % (i, omega))
                        if omega < 1.1:
                            omega = 1.0
                            break
                        else:
                            # half the distance to 1.0
                            omega = ((omega - 1.0) / 2.0) + 1.0

                info_green("(sub %s) Chose omega = %s" % (i, omega))
                x.isset(var, 0.0)
                x.isaxpy(var, 1.0, child_old_x)
                x.isaxpy(var, omega, child_diff)
                old_x.restoreSubVector(var, child_old_x)

            # compute norms
            x.normBegin()
            f.normBegin()
            y.zeroEntries()
            y.axpy(-1.0, old_x)
            y.axpy(1.0,  x)
            y.normBegin()
            xnorm = x.normEnd()
            fnorm = f.normEnd()
            ynorm = y.normEnd()

            if hasattr(self.problem, 'lb'):
                fnorm = get_residual_norm_vi(f, x, self.problem.lb, self.problem.ub)

            x.copy(old_x)

            its += 1
            snes.setIterationNumber(its)
            snes.reason = snes.callConvergenceTest(its, xnorm, ynorm, fnorm)
            snes.logConvergenceHistory(fnorm, lits)
            set_snes_linear_its(snes, lits)
            snes.monitor(its, fnorm)

        self.log.pop()

    def view(self, snes, viewer):
        tab = viewer.getTabLevel() + 2
        sp  = " " * tab

        def printx(msg):
            if PETSc.COMM_WORLD.rank == 0:
                print msg

        if not self.initialised:
            printx(sp + "Solver not initialised")
            return

        for (i, snes_) in enumerate(self.subsnes):
            printx(sp + "-" * 80)
            printx(sp + ("Viewing subsnes %d" % i))
            snes_.view(viewer)

    def check_bounds(self, x):
        # Given a global state x, check if it lies within the bounds
        # specified by the problem
        if hasattr(self.problem, 'lb'):
            assert hasattr(self.problem, 'ub')
            xa = x.getArray()
            lba = self.problem.lb.getArray()
            uba = self.problem.ub.getArray()

            out = True
            for (lbi, xi, ubi) in zip(lba, xa, uba):
                if not lbi <= xi <= ubi:
                    out = False
            # call MPI to let everyone know.
            out = MPI.COMM_WORLD.allreduce(out, op=MPI.MIN)
            return out
        else:
            return True
