# Copyright (C) 2015 Patrick Farrell
#
# This file is a supplemental material to the paper
# Linear and nonlinear solvers for variational phase-field models of brittle fracture
# by P. E. Farrell and C. Maurini
#
# varfrac-solvers is free software: you can redistribute it and/or modify
# it under the terms of the GNU Lesser General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# varfrac-solvers is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
# GNU Lesser General Public License for more details.
#
# You should have received a copy of the GNU Lesser General Public License
# along with varfrac-solvers. If not, see <http://www.gnu.org/licenses/>.

import sys

# ORAM with ideal linear solver
am_lu_args = [sys.argv[0]] + """
                       --petsc.snes_max_it 10000
                       --petsc.snes_type python
                       --petsc.snes_linesearch_type basic
                       --petsc.snes_stol 0.0
                       --petsc.snes_rtol 1.0e-10
                       --petsc.snes_atol 1.0e-7
                       --petsc.snes_monitor
                       --petsc.snes_converged_reason
                       --petsc.snes_max_funcs 1000000

                       --petsc.snes_linesearch_type basic
                       --petsc.snes_linesearch_damping 1.0
                       --petsc.snes_linesearch_monitor

                       --petsc.nfieldsplit_0_snes_max_it 100
                       --petsc.nfieldsplit_0_snes_type newtonls
                       --petsc.nfieldsplit_0_snes_linesearch_type basic
                       --petsc.nfieldsplit_0_snes_stol 0.0
                       --petsc.nfieldsplit_0_snes_rtol 1.0e-10
                       --petsc.nfieldsplit_0_snes_atol 1.0e-10
                       --petsc.nfieldsplit_0_ksp_type gmres
                       --petsc.nfieldsplit_0_pc_type lu
                       --petsc.nfieldsplit_0_pc_factor_mat_solver_package mumps

                       --petsc.nfieldsplit_1_snes_max_it 100
                       --petsc.nfieldsplit_1_snes_type vinewtonrsls
                       --petsc.nfieldsplit_1_snes_linesearch_type basic
                       --petsc.nfieldsplit_1_snes_stol 0.0
                       --petsc.nfieldsplit_1_snes_rtol 1.0e-10
                       --petsc.nfieldsplit_1_snes_atol 1.0e-10
                       --petsc.nfieldsplit_1_ksp_type gmres
                       --petsc.nfieldsplit_1_pc_type lu
                       --petsc.nfieldsplit_1_pc_factor_mat_solver_package mumps
                       """.split()

# ORAM with iterative linear solvers
am_ksp_args = [sys.argv[0]] + """
                       --petsc.snes_max_it 500
                       --petsc.snes_type python
                       --petsc.snes_linesearch_type basic
                       --petsc.snes_stol 0.0
                       --petsc.snes_rtol 1.0e-10
                       --petsc.snes_atol 1.0e-7
                       --petsc.snes_monitor
                       --petsc.snes_converged_reason
                       --petsc.snes_max_funcs 1000000

                       --petsc.nfieldsplit_0_snes_max_it 100
                       --petsc.nfieldsplit_0_snes_type newtonls
                       --petsc.nfieldsplit_0_snes_linesearch_type basic
                       --petsc.nfieldsplit_0_snes_stol 0.0
                       --petsc.nfieldsplit_0_snes_rtol 1.0e-10
                       --petsc.nfieldsplit_0_snes_atol 1.0e-10
                       --petsc.nfieldsplit_0_snes_monitor
                       --petsc.nfieldsplit_0_snes_converged_reason
                       --petsc.nfieldsplit_0_snes_linesearch_monitor
                       --petsc.nfieldsplit_0_ksp_type cg
                       --petsc.nfieldsplit_0_ksp_monitor_true_residual
                       --petsc.nfieldsplit_0_ksp_rtol 1.0e-10
                       --petsc.nfieldsplit_0_ksp_atol 1.0e-10
                       --petsc.nfieldsplit_0_pc_type gamg

                       --petsc.nfieldsplit_1_snes_max_it 100
                       --petsc.nfieldsplit_1_snes_type vinewtonrsls
                       --petsc.nfieldsplit_1_snes_linesearch_type basic
                       --petsc.nfieldsplit_1_snes_stol 0.0
                       --petsc.nfieldsplit_1_snes_rtol 1.0e-10
                       --petsc.nfieldsplit_1_snes_atol 1.0e-10
                       --petsc.nfieldsplit_1_snes_monitor
                       --petsc.nfieldsplit_1_snes_converged_reason
                       --petsc.nfieldsplit_1_snes_linesearch_monitor
                       --petsc.nfieldsplit_1_ksp_type cg
                       --petsc.nfieldsplit_1_ksp_monitor_true_residual
                       --petsc.nfieldsplit_1_pc_type ml
                       """.split()

# ORAM-N with fully iterative solvers
am_newton_ksp_args = [sys.argv[0]] + """
                       --petsc.snes_max_it 100
                       --petsc.snes_type composite
                       --petsc.snes_composite_type multiplicative
                       --petsc.snes_composite_sneses python,newtonls
                       --petsc.snes_linesearch_monitor
                       --petsc.snes_stol 1.0e-6
                       --petsc.snes_rtol 1.0e-9
                       --petsc.snes_atol 1.0e-7
                       --petsc.snes_monitor
                       --petsc.snes_converged_reason
                       --petsc.snes_max_fail 10000
                       --petsc.snes_max_funcs 1000000

                       --petsc.sub_0_snes_type python
                       --petsc.sub_0_snes_linesearch_type basic
                       --petsc.sub_0_snes_stol 0.0
                       --petsc.sub_0_snes_rtol 1.0e-3
                       --petsc.sub_0_snes_atol 1.0e-6
                       --petsc.sub_0_snes_max_it 10000
                       --petsc.sub_0_snes_monitor
                       --petsc.sub_0_snes_converged_reason

                       --petsc.sub_0_nfieldsplit_0_snes_max_it 100
                       --petsc.sub_0_nfieldsplit_0_snes_type newtonls
                       --petsc.sub_0_nfieldsplit_0_snes_linesearch_type basic
                       --petsc.sub_0_nfieldsplit_0_snes_stol 0.0
                       --petsc.sub_0_nfieldsplit_0_snes_rtol 1.0e-10
                       --petsc.sub_0_nfieldsplit_0_snes_atol 1.0e-9
                       --petsc.sub_0_nfieldsplit_0_snes_monitor
                       --petsc.sub_0_nfieldsplit_0_snes_converged_reason
                       --petsc.sub_0_nfieldsplit_0_ksp_type cg
                       --petsc.sub_0_nfieldsplit_0_ksp_atol 1.0e-10
                       --petsc.sub_0_nfieldsplit_0_ksp_rtol 1.0e-9
                       --petsc.sub_0_nfieldsplit_0_pc_type gamg

                       --petsc.sub_0_nfieldsplit_1_snes_max_it 100
                       --petsc.sub_0_nfieldsplit_1_snes_type vinewtonrsls
                       --petsc.sub_0_nfieldsplit_1_snes_linesearch_type basic
                       --petsc.sub_0_nfieldsplit_1_snes_stol 0.0
                       --petsc.sub_0_nfieldsplit_1_snes_rtol 1.0e-10
                       --petsc.sub_0_nfieldsplit_1_snes_atol 1.0e-9
                       --petsc.sub_0_nfieldsplit_1_snes_vi_monitor
                       --petsc.sub_0_nfieldsplit_1_snes_converged_reason
                       --petsc.sub_0_nfieldsplit_1_ksp_type cg
                       --petsc.sub_0_nfieldsplit_1_pc_type ml

                       --petsc.sub_1_snes_max_it 10
                       --petsc.sub_1_snes_type vinewtonrsls
                       --petsc.sub_1_snes_linesearch_type bt
                       --petsc.sub_1_snes_linesearch_maxstep 1.0
                       --petsc.sub_1_snes_linesearch_monitor
                       --petsc.sub_1_snes_stol 0.0
                       --petsc.sub_1_snes_rtol 1.0e-10
                       --petsc.sub_1_snes_atol 1.0e-7
                       --petsc.sub_1_snes_vi_monitor
                       --petsc.sub_1_snes_converged_reason
                       --petsc.sub_1_ksp_type minres
                       --petsc.sub_1_ksp_max_it 10000
                       --petsc.sub_1_ksp_monitor_true_residual
                       --petsc.sub_1_ksp_converged_reason
                       --petsc.sub_1_inner_pc_type fieldsplit
                       --petsc.sub_1_inner_pc_fieldsplit_type symmetric_multiplicative
                       --petsc.sub_1_inner_fieldsplit_0_ksp_type richardson
                       --petsc.sub_1_inner_fieldsplit_0_ksp_max_it 2
                       --petsc.sub_1_inner_fieldsplit_0_pc_type gamg
                       --petsc.sub_1_inner_fieldsplit_0_pc_factor_mat_solver_package mumps
                       --petsc.sub_1_inner_fieldsplit_1_ksp_type richardson
                       --petsc.sub_1_inner_fieldsplit_1_ksp_max_it 2
                       --petsc.sub_1_inner_fieldsplit_1_pc_type ml
                       --petsc.sub_1_inner_fieldsplit_1_pc_factor_mat_solver_package mumps
                       --petsc.sub_1_snes_max_fail 10000
                       """.split()

# ORAM-N with ideal linear solvers
am_newton_lu_args = [sys.argv[0]] + """
                       --petsc.snes_max_it 100
                       --petsc.snes_type composite
                       --petsc.snes_composite_type multiplicative
                       --petsc.snes_composite_sneses python,newtonls
                       --petsc.snes_linesearch_monitor
                       --petsc.snes_stol 1.0e-6
                       --petsc.snes_rtol 1.0e-9
                       --petsc.snes_atol 1.0e-7
                       --petsc.snes_monitor
                       --petsc.snes_converged_reason
                       --petsc.snes_max_fail 10000
                       --petsc.snes_max_funcs 1000000

                       --petsc.sub_0_snes_type python
                       --petsc.sub_0_snes_linesearch_type basic
                       --petsc.sub_0_snes_linesearch_damping 1.0 # overrelaxation
                       --petsc.sub_0_snes_stol 0.
                       --petsc.sub_0_snes_rtol 1.0e-3
                       --petsc.sub_0_snes_atol 1.0e-6
                       --petsc.sub_0_snes_max_it 10000
                       --petsc.sub_0_snes_monitor
                       --petsc.sub_0_snes_converged_reason

                       --petsc.sub_0_nfieldsplit_0_snes_max_it 100
                       --petsc.sub_0_nfieldsplit_0_snes_type newtonls
                       --petsc.sub_0_nfieldsplit_0_snes_linesearch_type basic
                       --petsc.sub_0_nfieldsplit_0_snes_stol 0.0
                       --petsc.sub_0_nfieldsplit_0_snes_rtol 1.0e-10
                       --petsc.sub_0_nfieldsplit_0_snes_atol 1.0e-9
                       --petsc.sub_0_nfieldsplit_0_snes_monitor
                       --petsc.sub_0_nfieldsplit_0_snes_converged_reason
                       --petsc.sub_0_nfieldsplit_0_ksp_type preonly
                       --petsc.sub_0_nfieldsplit_0_ksp_atol 1.0e-10
                       --petsc.sub_0_nfieldsplit_0_ksp_rtol 1.0e-9
                       --petsc.sub_0_nfieldsplit_0_pc_type lu
                       --petsc.sub_0_nfieldsplit_0_pc_factor_mat_solver_package mumps

                       --petsc.sub_0_nfieldsplit_1_snes_max_it 100
                       --petsc.sub_0_nfieldsplit_1_snes_type vinewtonrsls
                       --petsc.sub_0_nfieldsplit_1_snes_linesearch_type basic
                       --petsc.sub_0_nfieldsplit_1_snes_stol 0.0
                       --petsc.sub_0_nfieldsplit_1_snes_rtol 1.0e-10
                       --petsc.sub_0_nfieldsplit_1_snes_atol 1.0e-9
                       --petsc.sub_0_nfieldsplit_1_snes_vi_monitor
                       --petsc.sub_0_nfieldsplit_1_snes_converged_reason
                       --petsc.sub_0_nfieldsplit_1_ksp_type preonly
                       --petsc.sub_0_nfieldsplit_1_pc_type lu
                       --petsc.sub_0_nfieldsplit_1_pc_factor_mat_solver_package mumps

                       --petsc.sub_1_snes_max_it 10
                       --petsc.sub_1_snes_type vinewtonrsls
                       --petsc.sub_1_snes_linesearch_type bt
                       --petsc.sub_1_snes_linesearch_maxstep 1.0
                       --petsc.sub_1_snes_linesearch_monitor
                       --petsc.sub_1_snes_stol 0.0
                       --petsc.sub_1_snes_rtol 1.0e-10
                       --petsc.sub_1_snes_atol 1.0e-7
                       --petsc.sub_1_snes_vi_monitor
                       --petsc.sub_1_snes_converged_reason
                       --petsc.sub_1_ksp_type preonly
                       --petsc.sub_1_inner_pc_type lu
                       --petsc.sub_1_inner_pc_factor_mat_solver_package mumps
                       --petsc.sub_1_snes_max_fail 10000
                    """.split()

# ORAM-N with iterative outer linear solver and ideal inner solvers
am_newton_ksp_ideal_args = [sys.argv[0]] + """
                       --petsc.snes_max_it 100
                       --petsc.snes_type composite
                       --petsc.snes_composite_type multiplicative
                       --petsc.snes_composite_sneses python,newtonls
                       --petsc.snes_linesearch_monitor
                       --petsc.snes_stol 1.0e-6
                       --petsc.snes_rtol 1.0e-9
                       --petsc.snes_atol 1.0e-7
                       --petsc.snes_monitor
                       --petsc.snes_converged_reason
                       --petsc.snes_max_fail 10000
                       --petsc.snes_max_funcs 1000000

                       --petsc.sub_0_snes_type python
                       --petsc.sub_0_snes_linesearch_type basic
                       --petsc.sub_0_snes_linesearch_damping 1.0 # overrelaxation
                       --petsc.sub_0_snes_stol 0.
                       --petsc.sub_0_snes_rtol 1.0e-3
                       --petsc.sub_0_snes_atol 1.0e-6
                       --petsc.sub_0_snes_max_it 10000
                       --petsc.sub_0_snes_monitor
                       --petsc.sub_0_snes_converged_reason

                       --petsc.sub_0_nfieldsplit_0_snes_max_it 100
                       --petsc.sub_0_nfieldsplit_0_snes_type newtonls
                       --petsc.sub_0_nfieldsplit_0_snes_linesearch_type basic
                       --petsc.sub_0_nfieldsplit_0_snes_stol 0.0
                       --petsc.sub_0_nfieldsplit_0_snes_rtol 1.0e-10
                       --petsc.sub_0_nfieldsplit_0_snes_atol 1.0e-9
                       --petsc.sub_0_nfieldsplit_0_snes_monitor
                       --petsc.sub_0_nfieldsplit_0_snes_converged_reason
                       --petsc.sub_0_nfieldsplit_0_ksp_type preonly
                       --petsc.sub_0_nfieldsplit_0_ksp_atol 1.0e-10
                       --petsc.sub_0_nfieldsplit_0_ksp_rtol 1.0e-9
                       --petsc.sub_0_nfieldsplit_0_pc_type lu
                       --petsc.sub_0_nfieldsplit_0_pc_factor_mat_solver_package mumps

                       --petsc.sub_0_nfieldsplit_1_snes_max_it 100
                       --petsc.sub_0_nfieldsplit_1_snes_type vinewtonrsls
                       --petsc.sub_0_nfieldsplit_1_snes_linesearch_type basic
                       --petsc.sub_0_nfieldsplit_1_snes_stol 0.0
                       --petsc.sub_0_nfieldsplit_1_snes_rtol 1.0e-10
                       --petsc.sub_0_nfieldsplit_1_snes_atol 1.0e-9
                       --petsc.sub_0_nfieldsplit_1_snes_vi_monitor
                       --petsc.sub_0_nfieldsplit_1_snes_converged_reason
                       --petsc.sub_0_nfieldsplit_1_ksp_type preonly
                       --petsc.sub_0_nfieldsplit_1_pc_type lu
                       --petsc.sub_0_nfieldsplit_1_pc_factor_mat_solver_package mumps

                       --petsc.sub_1_snes_max_it 10
                       --petsc.sub_1_snes_type vinewtonrsls
                       --petsc.sub_1_snes_linesearch_type bt
                       --petsc.sub_1_snes_linesearch_maxstep 1.0
                       --petsc.sub_1_snes_linesearch_monitor
                       --petsc.sub_1_snes_stol 0.0
                       --petsc.sub_1_snes_rtol 1.0e-10
                       --petsc.sub_1_snes_atol 1.0e-7
                       --petsc.sub_1_snes_vi_monitor
                       --petsc.sub_1_snes_converged_reason
                       --petsc.sub_1_ksp_type minres
                       --petsc.sub_1_ksp_monitor_true_residual
                       --petsc.sub_1_ksp_converged_reason
                       --petsc.sub_1_inner_pc_type fieldsplit
                       --petsc.sub_1_inner_pc_fieldsplit_type symmetric_multiplicative
                       --petsc.sub_1_inner_fieldsplit_0_ksp_type preonly
                       --petsc.sub_1_inner_fieldsplit_0_pc_type lu
                       --petsc.sub_1_inner_fieldsplit_0_pc_factor_mat_solver_package mumps
                       --petsc.sub_1_inner_fieldsplit_1_ksp_type preonly
                       --petsc.sub_1_inner_fieldsplit_1_pc_type lu
                       --petsc.sub_1_inner_fieldsplit_1_pc_factor_mat_solver_package mumps
                       --petsc.sub_1_inner_pc_factor_mat_solver_package mumps
                       --petsc.sub_1_snes_max_fail 10000

                    """.split()
