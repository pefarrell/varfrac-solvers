# Copyright (C) 2015 Patrick Farrell, Corrado Maurini
#
# This file is a supplemental material to the paper
# Linear and nonlinear solvers for variational phase-field models of brittle fracture
# by P. E. Farrell and C. Maurini
#
# varfrac-solvers is free software: you can redistribute it and/or modify
# it under the terms of the GNU Lesser General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# varfrac-solvers is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
# GNU Lesser General Public License for more details.
#
# You should have received a copy of the GNU Lesser General Public License
# along with varfrac-solvers. If not, see <http://www.gnu.org/licenses/>.

from dolfin import *

from problems import VIForwardProblem
from nfieldsplit import NonlinearGaussSeidel
from petscsnessolver import PetscSnesSolver
import sys
from petsc4py import PETSc
import sympy
from petsctools import get_residual_norm_vi

# Set some common FEniCS flags
parameters["form_compiler"]["optimize"]     = True
parameters["form_compiler"]["cpp_optimize"] = True
parameters["form_compiler"]["representation"] = "quadrature"
parameters["form_compiler"]["cpp_optimize_flags"] = "-O3 -ffast-math -march=native"

# Some fiddling with I/O
from ufl import log
log.set_level(log.INFO)

if MPI.rank(mpi_comm_world()) > 0 and (sys.platform != "darwin"):
  sys.stdout = open("/dev/null", "w")
  # and handle C/C++ code as well
  from ctypes import *
  libc = CDLL("libc.so.6")
  stdout = libc.fdopen(1, "w")
  libc.freopen("/dev/null", "w", stdout)

def mylog(msg):
    print " PFCM: %s" % msg

def get_parameters(extras):
    app_par = Parameters("user")
    app_par.add("petsc_args", "am_lu_args", ["am_ksp_args", "am_lu_args", "am_newton_lu_args", "am_newton_ksp_args", "am_newton_ksp_ideal_args"])
    app_par.add("hratio", 4.)
    app_par.add("ell", 1.)
    app_par.add("savedir", "output")
    app_par.add("overrelaxation", 1.6)

    overridden = {}
    for key in extras:
        if key not in app_par:
            app_par.add(key, extras[key])
        else:
            overridden[key] = extras[key]

    app_par.update(overridden)

    parameters.add(app_par)
    # Parsing form command line and add appropiate petsc options
    parameters.parse()

    return parameters

class FractureProblem(VIForwardProblem):
    def __init__(self, energy, energy_z, Z, z, overrelaxation=1.0, bcs=None, bounds=None, P=None):
        VIForwardProblem.__init__(self, energy_z, Z, z, bcs=bcs, bounds=bounds, P=None)
        self.energy = energy
        self.overrelaxation = overrelaxation
        self.isets = self.compute_isets()

        fieldsplit_is = []
        for i in range(len(self.isets)):
            fieldsplit_is.append(("%s" % i, self.isets[i]))

        self.set_fieldsplit_is(fieldsplit_is)

    def compute_isets(self):
        Z = self.function_space
        variables = []
        for i in range(2):
            iset = PETSc.IS().createGeneral(Z.sub(i).dofmap().dofs())
            variables.append(iset)

        #  Now attach the near nullspace.
        if Z.mesh().geometry().dim() == 2:
            full_exprs = [Constant((1, 0, 0)), Constant((0, 1, 0)), Expression(("-x[1]", "x[0]", "0.0"))]
        elif Z.mesh().geometry().dim() == 3:
            full_exprs = [Constant((1, 0, 0, 0)), Constant((0, 1, 0, 0)), Constant((0, 0, 1, 0)),
                          Expression(("-x[1]", "x[0]", "0.0", "0.0")), Expression(("0.0", "-x[2]", "x[1]", "0.0")), Expression(("-x[2]", "0.0", "x[0]", "0.0"))]

        full_vecs  = [as_backend_type(interpolate(rbm, Z).vector()).vec() for rbm in full_exprs]

        sub_vecs = []
        for full_vec in full_vecs:
            sub_vec = full_vec.getSubVector(variables[0])
            sub_vecs.append(sub_vec.copy())
            full_vec.restoreSubVector(variables[0], sub_vec)

        # Gram-Schmidt orthogonalisation
        basis = []
        for i in range(len(sub_vecs)):
            orthogonal_vec = sub_vecs[i]
            for j in range(i):
                orthogonal_vec.axpy(-basis[j].dot(orthogonal_vec), basis[j]) # subtract off projections onto existing basis vectors
            orthogonal_vec.scale(1.0/orthogonal_vec.norm())
            basis.append(orthogonal_vec)

        nullsp = PETSc.NullSpace().create(vectors=basis)
        variables[0].compose("nearnullspace", nullsp)
        return variables

class FractureSolver(PetscSnesSolver):
    def __init__(self, problem, zvec, overrelaxation=None):
        PetscSnesSolver.__init__(self)
        self.setup(problem, zvec)

        variables = problem.isets

        if overrelaxation is None:
            if hasattr(problem, 'overrelaxation'):
                overrelaxation = problem.overrelaxation
            else:
                overrelaxation = 1.0

        if self.snes.getType() == "composite":
            sub_0 = self.snes.getCompositeSNES(0)
            sub_0.setType("python")
            sub_0.setPythonContext(NonlinearGaussSeidel(problem, variables, overrelaxation=overrelaxation))
        else:
            self.snes.setType("python")
            self.snes.setPythonContext(NonlinearGaussSeidel(problem, variables, overrelaxation=overrelaxation))

