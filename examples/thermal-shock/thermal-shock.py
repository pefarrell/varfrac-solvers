# Copyright (C) 2015 Patrick Farrell, Corrado Maurini
#
# This file is a supplemental material to the paper
# Linear and nonlinear solvers for variational phase-field models of brittle fracture
# by P. E. Farrell and C. Maurini
#
# varfrac-solvers is free software: you can redistribute it and/or modify
# it under the terms of the GNU Lesser General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# varfrac-solvers is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
# GNU Lesser General Public License for more details.
#
# You should have received a copy of the GNU Lesser General Public License
# along with varfrac-solvers. If not, see <http://www.gnu.org/licenses/>.

import sys
sys.path.append('../../fracture/')
import os
import petsc4py
from dolfin import *
from petsc4py import PETSc
from numpy import linspace
from fracture import *
from petscsettings import *
import mesher

# set and parse the parameters
parameters = get_parameters(extras={"L": 20.0, "H": 5.0, "intensity": 4.0, "tmin": 0.0001, "tmax": 2.5, "nsteps": 25, "meshdir": "meshes"})
pu = parameters.user
info("Using %s" %pu.petsc_args)
args = eval(pu.petsc_args)
if len(sys.argv) > 1:
    args += sys.argv[1:]
parameters.parse(argv=args)

# Create the directory to save the results and move there
savedir = pu.savedir
File(savedir + "/parameters.xml") << parameters
os.chdir(savedir)
alphas = File("damage.pvd")
displs = File("displacement.pvd")
eps0ts = File("eps0.pvd")
petsc4py.init(['-log_summary', 'summary.log'])


# Material parameters
ell = Constant(pu.ell) # internal length
E = Constant(1.0) # Young modulys
nu = Constant(0.3) # Poisson ratio
mu = Constant(E/(2.0*(1.0 + nu)))
lmbda = Constant(E*nu/(1.0 - nu**2))
Gc = Constant(1.0) # Toughness
k_ell = Constant(1.0e-8) # Residual stiffness

# Loading
DeltaTc = sqrt(3./(8.*ell)) # Critical loading (result valid only for w(alpha) = alpha)
DeltaT = Constant(-pu.intensity*DeltaTc) # Temperature drop at the surface

# Generate the mesh and boundary indicators for BC's
# (a dedicated function in the mesher module is used)
# Geometry
L = pu.L
H = pu.H
cell_size = pu.ell/pu.hratio
mesh, boundaries = mesher.make_mesh(L, H, cell_size)
ndim = mesh.geometry().dim()
mesh_info = {"cells": mesh.num_cells(), "vertices": mesh.num_vertices(), "h_min": mesh.hmin(), "h_max": mesh.hmax()}

# Set the FE function space (discretisation)
V = VectorFunctionSpace(mesh, "CG", 1) # Space for displacement vector
Q = FunctionSpace(mesh, "CG", 1) # Space for damage field
Z = MixedFunctionSpace([V, Q]) # Composition of the two function spaces
mylog("Number of degrees of freedom: %s = (%s, %s)" % (Z.dim(), Z.sub(0).dim(), Z.sub(1).dim()))
ndof = {Z.dim(), Z.sub(0).dim(), Z.sub(1).dim()}

# Define the functions
z = Function(Z) # The function in the full space
(u, alpha) = split(z) # u and alpha are displacements and damage, respectively
z_test = TestFunction(Z)

# Define the consitutive function of the damage model
w = lambda alpha: alpha # energy dissipation
a = lambda alpha: (1-alpha)**2 # stiffness modulation

# Normalization constant for the dissipated energy
# to get Griffith surface energy for l going to zero.
xi = sympy.Symbol("xi")
c_w = float(4*sympy.integrate(sympy.sqrt(w(xi)),(xi,0,1)))

# Strain and stress
eps = lambda v: sym(grad(v)) # linearized strain tensor
sigma_0 = lambda eps: 2.0*mu*(eps) + lmbda*tr(eps)*Identity(ndim) # undamaged stress
sigma = lambda eps, alpha: (a(alpha) + k_ell)*sigma_0(eps) # stress accounting for damage

# Inelastic strain due to the temperature field
# We use here analytical solution for a thermal problem in a homogenous semi-infinity slab
# tau is a normalized time parameter (square root of the real time divided by the thermal diffusivity)
eps_0t  = Expression("DeltaT * erfc((x[1]/ell)/tau)", tau = 0.0, DeltaT =  DeltaT, ell = ell)
eps_0   = Identity(ndim) * eps_0t

# Energies
elastic_energy = 0.5*inner(sigma(eps(u) - eps_0, alpha), eps(u) - eps_0)*dx
dissipated_energy = Gc/c_w*(w(alpha)/ell + ell*dot(grad(alpha), grad(alpha)))*dx
energy = elastic_energy + dissipated_energy

# First directional derivative of the energy (residual)
energy_z = derivative(energy, z, z_test)

# Dirichlet BC's on displacement
# Note: here we do not to impose corresponding Dirichlet BC's on damage
bc_u1 = DirichletBC(Z.sub(0).sub(0), 0, boundaries, 1) # sliding at left (1)
bc_u2 = DirichletBC(Z.sub(0).sub(0), 0, boundaries, 2) # sliding at rigth (2)
bc_u3 = DirichletBC(Z.sub(0).sub(1), Constant(0.), boundaries, 3) # sliding at top (3)
bcs = [bc_u1, bc_u2, bc_u3]

# Define the upper and lower bounds for the damage field
inf = 1.0e20
lb = Function(Z); ub = Function(Z)
lb.interpolate(Constant((-inf, -inf, 0.0)))
ub.interpolate(Constant((+inf, +inf, 1.0)))

# Set the time discretisation
loadings = linspace(pu.tmin, pu.tmax, pu.nsteps)
for (i, load) in enumerate(loadings):
    load_timer = Timer("Load step %s" % load)
    info("Considering load: %s" % load)
    # Update the inelastic strain (temperature)
    eps_0t.tau = load

    # Solve the fracture problem
    problem = FractureProblem(energy, energy_z, Z, z, bcs=bcs, overrelaxation=pu.overrelaxation, bounds=(lb.vector(), ub.vector()))
    solver = FractureSolver(problem, z.vector())
    solver.solve(problem, z.vector())
    info_green("Solver converged for load = %s" % load)

    # Save to files
    File("state-%s.xml.gz" % load) << z
    (u, alpha) = z.split(deepcopy=True)
    u.rename("Displacement", "Displacement")
    eps0t_f = interpolate(eps_0t, FunctionSpace(Z.mesh(), 'CG', 1))
    alpha.rename("Damage", "Damage")
    eps0ts << (eps0t_f, load)
    displs << (u, load)
    alphas << (alpha, load)
    eps0t_f.rename("eps0", "eps0")

    # Update
    assign(lb.sub(1), alpha)
    # Plot(alpha, mode = "color")
    time = load_timer.stop()
    mylog("Time for load step %s: %s seconds" % (load, time))
