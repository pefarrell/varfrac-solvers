# Copyright (C) 2015 Corrado Maurini
#
# This file is a supplemental material to the paper
# Linear and nonlinear solvers for variational phase-field models of brittle fracture
# by P. E. Farrell and C. Maurini
#
# varfrac-solvers is free software: you can redistribute it and/or modify
# it under the terms of the GNU Lesser General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# varfrac-solvers is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
# GNU Lesser General Public License for more details.
#
# You should have received a copy of the GNU Lesser General Public License
# along with varfrac-solvers. If not, see <http://www.gnu.org/licenses/>.

from dolfin import *
from mshr import *

def make_mesh(L, H, cell_size):
    """
    Create the mesh and the meshfunction with boudary numbering for BC's
    The mesh is unstructured and uniform
    input :
      - L, H : geometric dimensions
      - cell_size: target cell size
    output:
      - mesh: the mesh
      - meshfunction: the mesh function with the boundary indicators
    """
    geom = Rectangle(Point(0., 0.), Point(L, H))
    mesh = Mesh()
    mesh_generator = CSGCGALMeshGenerator2D()
    mesh_generator.parameters["mesh_resolution"] = -1.
    mesh_generator.parameters["cell_size"] = cell_size
    mesh_generator.generate(geom, mesh)

    class Left(SubDomain):
        def inside(self, x, on_boundary):
            return near(x[0] * 0.01, 0)

    class Right(SubDomain):
        def inside(self, x, on_boundary):
            return near((x[0] - L) * 0.01, 0.)

    class Top(SubDomain):
        def inside(self, x, on_boundary):
            return near(x[1], H)

    class Bottom(SubDomain):
        def inside(self, x, on_boundary):
            return near(x[1], 0.)

    # Initialize sub-domain instances
    left = Left()
    right = Right()
    top = Top()
    bottom = Bottom()

    # Define MeshFunction to identify boundaries by numbers
    boundaries = FacetFunction("size_t", mesh)
    boundaries.set_all(0)
    left.mark(boundaries, 1)   # mark left as 1
    right.mark(boundaries, 2)  # mark right as 2
    top.mark(boundaries, 3)    # mark top as 3
    bottom.mark(boundaries, 4) # mark bottom as 4
    return (mesh, boundaries)



if __name__ == '__main__':
    p = parameters
    up = Parameters("user")
    up.add("L", 20.)
    up.add("H", 8.)
    up.add("cellsize", 0.1)
    p.add(up)
    p.parse()
    meshname = "meshes/slab-%4.2f-%4.2f-%1.4f.xdmf"%(p.user.L,p.user.H,p.user.cellsize)
    info("------- Generating the mesh %s "%(meshname))
    mesh = make_mesh(p.user.L, p.user.H, p.user.cellsize)
    info("------- The mesh has %s vertices"%(mesh.num_vertices()))
    meshfile = File(meshname)
    meshfile << mesh

